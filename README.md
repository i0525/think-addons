# think-addons

## 介绍
用于API接口开发 

## 使用说明

### 功能介绍
所有的插件都要按照规定编写
####  目录结构

```
www  WEB部署目录（或者子目录）
├─addons                     插件目录
│  ├─addons_name             公共模块目录（可以更改）
│  │  ├─controller           控制器目录
│  │  │   ├─Index.php        模块函数文件
│  │  ├─model                模型目录
│  │  ├─config.php           插件动态配置
│  │  ├─info.ini             插件配置 （插件的安装，开关，基础信息）
│  │  ├─addons_name.php      插件方法（首字符大写 里面写的方法都是Hook）
|  |  └─ 
|  └─
└─
```
#### 基础方法

##### common.php 公共方法
###### 写入配置文件 config.php
> set_addon_config(插件名,配置信息)

> set_addon_fullconfig(插件名,配置信息)
###### 设置基础配置信息 info.ini
> set_addon_info(插件名,配置信息)
###### 获取插件类的配置数组 config.php
> get_addon_fullconfig(插件名)
###### 读取插件的基础信息 info.ini
> get_addon_info(插件名)
###### 输出Json + 停止一下操作
> dd(code,msg)
###### 输出Json + bool返回
> msg(bool,title)
###### 获取插件单例 - 使用抽象方法
> get_addon_instance(插件名)

#### Hook方法

##### auth认证开始 
> authStart | request
##### auth认证主体 
> authMain | request
##### auth认证结束 
> authEnd | request







