<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/2/20
 * Time: 22:05
 */

namespace think\addons;


use think\App;
use think\facade\Config;
use think\facade\Request;

class Controller extends \think\Controller
{
    // 当前插件操作
    protected $addon = null;
    protected $controller = null;
    protected $action = null;

    public function __construct($request = null)
    {
        if (is_null($request)) {
            $request = Request::instance();
        }
        // 生成request对象
        $this->request = $request;

        //移除HTML标签
        $this->request->filter('strip_tags');

        // 是否自动转换控制器和操作名
        $convert = Config::get('app.url_convert');

        $filter = $convert ? 'strtolower' : 'trim';

        // 处理路由参数
        $param = $this->request->param();

        $dispatch = (array)$this->request->dispatch();

        $var = isset($dispatch['var']) ? $dispatch['var'] : [];

        $var = array_merge($param, $var);

        if (isset($dispatch['method']) && substr($dispatch['method'][0], 0, 7) == "\\addons") {
            $arr = explode("\\", $dispatch['method'][0]);
            $addon = strtolower($arr[2]);
            $controller = strtolower(end($arr));
            $action = $dispatch['method'][1];
        } else {
            $addon = isset($var['addon']) ? $var['addon'] : '';
            $controller = isset($var['controller']) ? $var['controller'] : '';
            $action = isset($var['action']) ? $var['action'] : '';
        }

        $this->addon = $addon ? call_user_func($filter, $addon) : '';
        $this->controller = $controller ? call_user_func($filter, $controller) : 'index';
        $this->action = $action ? call_user_func($filter, $action) : 'index';



        if (!$this->addon) {
            //解析插件名
            $data = explode('\\', get_class($this));
            $this->addon = strtolower($data[1]);
        }


        // 父类的调用必须放在设置模板路径之后
        parent::__construct();
    }




}