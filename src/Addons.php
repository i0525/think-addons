<?php
/**
 * Created by PhpStorm.
 * User: 宏宇
 * Date: 2019/2/20
 * Time: 21:35
 */

namespace think;

use think\facade\Config;

abstract class Addons
{
    protected $infoRange = "addonInfo";
    protected $configRange = "addonConfig";
    public $addons_path = '';
    public function __construct()
    {
        //当前插件名
        $name = $this->getName();
        //当前插件目录
        $this->addons_path = ADDON_PATH . $name . DS;
    }

    /**
     * 读取基础配置信息
     * @param string $name
     * @return array
     */
    final public function getInfo($name = '')
    {
        if (empty($name)) {
            $name = $this->getName();
        }

        $info = Config::get($this->infoRange . $name);

        if ($info && is_array($info)) {
            return $info;
        }

        $info_file = $this->addons_path . 'info.ini';

        if (is_file($info_file)) {
            $info = Config::parse($info_file, '', $this->infoRange . $name);

        }
        Config::set($this->infoRange . $name, $info);
        return $info ? $info : [];
    }
    /**
     * 获取当前模块名
     * @return string
     */
    final public function getName()
    {
        $data = explode('\\', get_class($this));
        return strtolower(array_pop($data));
    }

    /**
     * 获取插件的配置数组
     * @param string $name 可选模块名
     * @return array
     */
    final public function getConfig($name = '')
    {
        if (empty($name)) {
            $name = $this->getName();
        }
        $config = Config::get($this->configRange . $name);

        if ($config) {
            return $config;
        }
        $config_file = $this->addons_path . 'config.php';

        if (is_file($config_file)) {
            $temp_arr = include $config_file;
            foreach ($temp_arr as $key => $value) {
                $config[$value['name']] = $value['value'];
            }
            unset($temp_arr);
        }
        Config::set($this->configRange . $name, $config);
        return $config;
    }
    /**
     * 设置配置数据
     * @param $name
     * @param array $value
     * @return array
     */
    final public function setConfig($name = '', $value = [])
    {
        if (empty($name)) {
            $name = $this->getName();
        }
        $config = $this->getConfig($name);
        $config = array_merge($config, $value);
        Config::set($this->configRange . $name, $config);
        return $config;
    }
    /**
     * 设置插件信息数据
     * @param $name
     * @param array $value
     * @return array
     */
    final public function setInfo($name = '', $value = [])
    {
        if (empty($name)) {
            $name = $this->getName();
        }
        $info = $this->getInfo($name);
        $info = array_merge($info, $value);
        Config::set($this->infoRange . $name, $info);
        return $info;
    }
    /**
     * 获取完整配置列表
     * @param string $name
     * @return array
     */
    final public function getFullConfig($name = '')
    {
        $fullConfigArr = [];
        if (empty($name)) {
            $name = $this->getName();
        }
        $config_file = $this->addons_path . 'config.php';
        if (is_file($config_file)) {
            $fullConfigArr = include $config_file;
        }
        return $fullConfigArr;
    }
    /**
     * 检查基础配置信息是否完整
     * @return bool
     */
    final public function checkInfo()
    {
        $info = $this->getInfo();

        $info_check_keys = ['name', 'title', 'intro', 'author', 'version', 'state'];
        foreach ($info_check_keys as $value) {
            if (!array_key_exists($value, $info)) {
                return false;
            }
        }
        return true;
    }
    //必须 - 插件安装
    abstract public function install();
    //必须 - 插件卸载
    abstract public function delete();
    //必须 - 插件启用
    abstract public function enable();
    //必须 - 插件禁用
    abstract public function disable();
}